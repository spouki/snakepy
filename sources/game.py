# -*- coding: utf-8 -*-
#/env/bin python

import pygame
import os
import time
import random
from snake import Snake
from musicPlayer import MusicPlayer

class Game:
    def __init__(self):
        print('Constructed Game Class')

    def locals(self):
        self.screen_width = 800
        self.screen_height = 600
        self.mainSurface = pygame.display.set_mode((self.screen_width,self.screen_height))
        self.gameState = 0
        self.gameStateRunning = False
        self.toRender = []
        self.background = pygame.Surface(self.mainSurface.get_size())
        self.background.fill((0,0,0))
        self.background = self.background.convert()
        self.fontHelper = pygame.font.SysFont('Fira Code Retina',40)
        self.playState = 0
        self.music = MusicPlayer()
        self.music.add('assets/menu.mp3')


    '''
    GameState :
        0 : Initing 
        1 : Menu
        2 : Game
        3 : Score
        4 : Quitting
    '''

    def init(self):
        print('Initiating game core')
        pygame.init()
        pygame.font.init()
        pygame.register_quit(self.quit())
        self.locals()

    def keyHandling(self):
        pass

    def gameLoop(self):
        self.music.play()
        clock = pygame.time.Clock()
        mainloop = True
        timer = time.perf_counter()
        continuePressed = False
        newGame = True
        direction = 0
        hasFood = False
        foodPos = ()
        pause = False
        item = {'type':'food','pts':5,'speed':0.05}

        while mainloop:
            pos = (-1,-1)
            # Key events
            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONUP:
                    pos = event.pos
                    # print('Mouse click : {}'.format(pos))
                if event.type == pygame.QUIT:
                    mainloop = False
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        if continuePressed == False and self.gameState == 0:
                            continuePressed = True
                            self.gameState = 1
                    if self.gameState == 2:
                        if event.key == pygame.K_UP and direction != 1:
                            direction = 3
                        elif event.key == pygame.K_RIGHT and direction != 2:
                            direction = 0
                        elif event.key == pygame.K_DOWN and direction != 3:
                            direction = 1
                        elif event.key == pygame.K_LEFT and direction != 0:
                            direction = 2
                        elif event.key == pygame.K_ESCAPE:
                            if self.playState == 2:
                                self.playState = 1
                            else:
                                self.playState = 2
                            pause = not pause
                    # if event.key == pygame.K_ESCAPE:
                    #     mainloop = False

            # Calculating displays
            self.mainSurface.blit(self.background,(0,0))
            if self.gameState == 0:
                elapsed = time.perf_counter() - timer
                if continuePressed == False:
                    self.gameStateRunning = True
                    launchSurface = pygame.image.load(os.path.join('assets','launch.png')).convert()
                    launchSurface.convert_alpha()
                    pressText = self.fontHelper.render('Press space to continue...',40,(255,255,255))
                    pressText.convert()
                    #Alpha proportionally to time
                    #0 sec == black, 3 sec == full colored
                    if elapsed <= 3:
                        launchSurface.set_alpha((elapsed/3.0)*255)
                        pressText.set_alpha((elapsed/3.0)*255)
                    #Mid positioning
                    screenMid = (self.screen_height/2,self.screen_width/2)
                    imgMid = (launchSurface.get_height()/2,launchSurface.get_width()/2)
                    # print('Surface sizes {} x {}'.format(launchSurface.get_height(),
                    self.mainSurface.blit(launchSurface,(screenMid[1]-imgMid[1],
                                                         screenMid[0]-imgMid[0]))
                    #Bottom press positioning
                    # print('{}'.format(elapsed%2))
                    if elapsed % 2 >= 0.50 and elapsed % 2 <= 1.00 or elapsed % 2 >= 1.50 and elapsed % 2 <= 2.00:
                        screenBottom = (self.screen_height,self.screen_width/2)
                        footerBottom = (pressText.get_height(),pressText.get_width()/2)
                        self.mainSurface.blit(pressText,(screenBottom[1]-footerBottom[1],
                                                         screenBottom[0]-footerBottom[0]))
                    # print('{} - {} = {}'.format(timer,time.perf_counter(),elapsed))
                #Booting, should take 3seconds, then display a "Press button" screen
                # print('Loaded image and charged in mainSurface')
            elif self.gameState == 1:
                #Menu
                #Selector
                #1.Game
                gameText = self.fontHelper.render('Play',40,(255,255,255))
                gameText.convert()
                gameBtn = pygame.draw.rect(self.mainSurface,(128,128,128),
                                           (15,15,gameText.get_width(),gameText.get_height()))
                #2.Scores
                scoresText = self.fontHelper.render('Scoreboard',40,(255,255,255))
                scoresText.convert()
                scoresBtn = pygame.draw.rect(self.mainSurface,(128,128,128),
                                           (15,150,scoresText.get_width(),scoresText.get_height()))
                #3.Quit
                quitText = self.fontHelper.render('Quit game',40,(255,255,255))
                quitText.convert()
                quitBtn = pygame.draw.rect(self.mainSurface,(128,128,128),
                                           (15,300,quitText.get_width(),quitText.get_height()))
                #
                #Display texts
                self.mainSurface.blit(gameText,(15,15))
                self.mainSurface.blit(scoresText,(15,150))
                self.mainSurface.blit(quitText,(15,300))

                #Check to get mouse click & position
                if pos != (-1,-1):
                    if gameBtn.collidepoint(pos):
                        self.gameState = 2
                        newGame = True
                    elif scoresBtn.collidepoint(pos):
                        self.gameState = 3
                    elif quitBtn.collidepoint(pos):
                        self.gameState = 4
            elif self.gameState == 2:
                #Game
                if newGame == True:
                    self.playState = 1 # PLAY STATE
                    direction = 0
                    board = pygame.draw.rect(self.mainSurface,(255,255,255),(5,5,self.mainSurface.get_width()-10,self.mainSurface.get_height()-10),1)
                    foodPos = (random.randrange(20,self.mainSurface.get_width()-20),random.randrange(20,self.mainSurface.get_height()-20))
                    foodItem = pygame.draw.rect(self.mainSurface,(200,20,20),(foodPos[1],foodPos[0],5,5))
                    spawnPos = (board.w/2-5,board.h/2-5)
                    snake = Snake(spawnPos,self.mainSurface)
                    newGame = False
                else:
                    if self.playState == 2: # PAUSE STATE
                        pauseText = self.fontHelper.render('Paused game',40,(255,255,255))
                        pauseText.convert()
                        self.mainSurface.blit(pauseText,(50,180))
                        restartText = self.fontHelper.render('Restart game',40,(255,255,255))
                        restartText.convert()
                        restartBtn = pygame.draw.rect(self.mainSurface, (128, 128, 128),
                                                   (100, 250, restartText.get_width(), restartText.get_height()))
                        self.mainSurface.blit(restartText,(100,250))
                        quitText = self.fontHelper.render('Quit game',40,(255,255,255))
                        quitText.convert()
                        quitBtn = pygame.draw.rect(self.mainSurface, (128, 128, 128),
                                                   (100, 350, quitText.get_width(), quitText.get_height()))
                        self.mainSurface.blit(quitText,(100,350))

                        if pos != (-1, -1):
                            if restartBtn.collidepoint(pos):
                                newGame = True
                                self.gameState = 2
                                self.playState = 0
                            elif quitBtn.collidepoint(pos):
                                self.gameState = 1
                    elif self.playState == 3: # END GAME STATE (aka GameOver)
                        endText = self.fontHelper.render('Game Over', 40,(255,255,255))
                        endText.convert()
                        self.mainSurface.blit(endText,(50,180))
                        score = str(snake.getScores()) + 'pts'
                        scoreText = self.fontHelper.render(score,40,(255,255,255))
                        self.mainSurface.blit(scoreText,(50,250))
                    else:
                        board = pygame.draw.rect(self.mainSurface,(255,255,255),
                                                 (5,5,self.mainSurface.get_width()-10,
                                                  self.mainSurface.get_height()-10),1)
                        #Snake has a direction.
                
                        #When eat something, grow for one square and get more speed
                        #When snake eat tail, then die
                        #When snake collide with border, then die

                        snake.move(direction)
                        if snake.collide(foodItem,item):
                            snake.ate(item['pts'],item['speed'])
                            foodPos = (random.randrange(20,self.mainSurface.get_height()-20),random.randrange(20,self.mainSurface.get_width()-20))
                            print('New food pos {}'.format(foodPos))
                        foodItem = pygame.draw.rect(self.mainSurface,(200,20,20),(foodPos[1],foodPos[0],5,5))

                        if snake.eatItself():
                            self.playState = 3 # END GAME STATE
                        snake.display()




                        # print('Snake is at x : {} - y : {}'.format(snake.x,snake.y))
            elif self.gameState == 3:
                #Scoreboard
                pass
            elif self.gameState == 4:
                #Quitting screen
                print('should quit')
                # pygame.quit()
                mainloop = False
                # pass
            #Refreshing display
            pygame.display.update()
            clock.tick(60)
            # pygame.time.delay(60)

    def quit(self):
        # pygame.quit()
        print('Quitting')
