# -*- coding: utf-8 -*-

# /env/bin python

from game import Game 
import os
'''
Snake
2D
a grid
a moving snake head
randomly spawned apples

'''


def main():
    print(os.getcwd())  # Prints the current working directory
    sources = os.getcwd().find('sources')
    if sources != -1:
        base_dir = os.getcwd()[:os.getcwd().rfind('/')]
    else:
        base_dir = os.getcwd()
    os.chdir(base_dir)
    print(os.getcwd())  # Prints the current working directory
#    exit()
    game_engine = Game()
    game_engine.init()
    game_engine.gameLoop()
    game_engine.quit()


if __name__ == '__main__':
    main()
