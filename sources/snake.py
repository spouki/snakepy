# -*- coding: utf-8 -*-
#/env/bin python

import pygame
from pygame.locals import *
import random
import copy


class Snake:
    def __init__(self,pos,gameBoardSurface):
        self.board = gameBoardSurface
        self.color = (20,200,20)

        self.pos = {'x':pos[0],'y':pos[1]}
        self.displaySize = 10

        self.size = 0
        self.speed = 1.0
        self.maxSpeed = 4.0
        self.points = 0

        self.queue = []

        self.direction = 0
        self.addNewQueueElem()

    def addNewQueueElem(self):
        if len(self.queue) == 0:
            self.queue.append(pygame.draw.rect(self.board,self.color,(self.pos['x'],self.pos['y'],self.displaySize,self.displaySize)))
        else:
            lastElem = self.getLastQueue()
            self.queue.append(pygame.draw.rect(self.board,self.color,(lastElem.x,lastElem.y,self.displaySize,self.displaySize)))

    def move(self,direction):
        if direction == 0:
            self.pos['x']+=5*self.speed
        elif direction == 1:
            self.pos['y']+=5*self.speed
        elif direction == 2:
            self.pos['x']-=5*self.speed
        elif direction == 3:
            self.pos['y']-=5*self.speed
        x = self.pos['x']
        y = self.pos['y']
        for q in range(len(self.queue)):
            tmpX = self.queue[q].x
            tmpY = self.queue[q].y
            self.queue[q].x = copy.copy(x)
            self.queue[q].y = copy.copy(y)
            x = copy.copy(tmpX)
            y = copy.copy(tmpY)
            

    def display(self):
        for q in range(len(self.queue)):
                self.queue[q] = pygame.draw.rect(self.board,self.color,(self.queue[q].x,self.queue[q].y,self.displaySize,self.displaySize))

    def collide(self,rect,item):
        if rect.colliderect(self.queue[0]):
            return True

    def ate(self,pts,speed):
        self.size+=1
        self.points+=pts
        if self.speed+speed < self.maxSpeed:
            self.speed+=speed
        self.addNewQueueElem()


    def getLastQueue(self):
        if len(self.queue) == 0:
            return None
        if len(self.queue) == 1:
            return self.queue[0]
        return self.queue[len(self.queue)-1]

    def getLastPos(self):
        if len(self.lastPos) == 1:
            return self.lastPos[0]
        return self.lastPos[len(self.lastPos)-1]

    def eatItself(self):
        lenQ = len(self.queue)
        if lenQ <= 2:
            return False
        elif lenQ > 2:
            for i in range(2,len(self.queue)):
                if self.queue[0].contains(self.queue[i]):
                    print('{} - {} - {}'.format(i,(self.queue[i].x,self.queue[i].y),(self.queue[0].x,self.queue[0].y)))
                    return True
        return False

    def getScores(self):
        return self.points
